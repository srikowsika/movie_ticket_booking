import React, { Component } from 'react';
import '../assets/stylesheets/stylesheet.css';
import { Link, Redirect } from "react-router-dom";
import axios from 'axios';
import config from '../config/config';

class Signin extends Component {

  constructor(props) {
    super(props);
    this.state = {
      username: " ",
      password: " ",
      userType: '',
      submitted: false,
      status: null,
    }

  }

  loginUser(event) {
    event.preventDefault();
    var user = {
      "username": this.state.username,
      "password": this.state.password
    }
    axios.post(`${config.url.baseURL}/login`, { user })
      .then(function (response) {
        if (response.status === 200) {
          localStorage.setItem("usertype", response.data.userType);
          localStorage.setItem("userId", response.data.userId);
          localStorage.setItem("token", response.data.token);
          localStorage.setItem("userName", response.data.userName);
          this.setState({
            status: true,
            userType: response.data.userType,
          })
        }
      }.bind(this))
      .catch((err) => {
        this.setState({
          status: false,
        })
      });

  }
  render() {

    if (this.state.status === false) {
      return (<Signin value='incorrect' />)
    }
    if (this.state.userType === 1) {
      return (<Redirect exact to='/admin/dashboard' />)
    }
    if (this.state.userType === 0) {
      return (<Redirect exact to="/movie" />)

    }
    return (
      <div>
        {this.props.value === '1' ?
          <div className="alert alert-success alert-dismissible fade show" role="alert">
            <p className='text'>Registeration Success! Please Login </p>
            <button type="button" className="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div> : null}
        <div className='inputForm'>
          <div className="card border-light mb-3" id='inputFormCard'>
            <div className="card-header" id='inputFormHeader'>
              Login Form
              </div>
            <div className="card-body border-top border-bottom-0" id='inputFormBody'>
              <form onSubmit={this.loginUser.bind(this)}>
                <div className="form-group">
                  <label>Username</label>
                  <input type="email" className='form-control' name="email" placeholder="Enter your username" onChange={event => this.setState({ username: event.target.value })} required />
                </div>
                <div className="form-group">
                  <label>Password</label>
                  <input type='password' className='form-control' onChange={event => this.setState({ password: event.target.value })} name='password' placeholder="Enter the Password"
                    required />
                </div>

                {this.props.value === 'incorrect' ? <p className='errorMessage'>Invalid Username or password</p> : null}
                <button className="btn btn-primary" id='userbutton'
                >Signin</button>
                <legend></legend>
                {this.props.history !== undefined ?
                  <p className='text'>Not a member?<Link to='/signup'> Register</Link></p> : null}



              </form>
            </div>
          </div>
        </div>
      </div>);
  }
}
export default Signin
