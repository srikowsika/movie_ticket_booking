import React from 'react';
import { Link } from 'react-router-dom';
import '../assets/stylesheets/stylesheet.css';

function Dashboard() {

  return (
    <div>
      <div className='container'>
        <div className='row'>
          <div className="col m-4 col-lg-3" id='adminCard'>
            <div className="card h-70">
              <Link to='/admin/city'><img className="card-img-top" src={require('../assets/images/city.jpeg')} alt="Cardcap" />
                <div className='card-body'><div className="card-title" id='inputFormHeader'>City</div></div></Link>
            </div>
          </div>
          <div className="col m-4 col-lg-3" id='adminCard'>
            <div className="card h-70">
              <Link to='/admin/theaterList'>  <img className="card-img-top" src={require('../assets/images/theater.jpeg')} alt="Cardcap" />
                <div className='card-body'><div className="card-title" id='inputFormHeader'>Theater</div></div>
              </Link></div>
          </div>
          <div className="col ml-0 m-4 col-lg-3" id='adminCard'>
            <div className="card h-70">
              <Link to='/admin/movie'> <img className="card-img-top" src={require('../assets/images/movie.jpg')} alt="Cardcap" />
                <div className='card-body'><div className="card-title" id='inputFormHeader'>Movie</div></div>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>);




}

export default Dashboard;