import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import '../assets/stylesheets/stylesheet.css';
import randomstring from 'randomstring';
import config from '../config/config';

class Ticket extends Component {

      constructor(props) {
            super(props);

            this.state = {
                  bookingId: localStorage.getItem("bookingId"),
                  userId: localStorage.getItem('userId'),
                  userName: localStorage.getItem('userName'),
                  ticketDetail: {},
                  status: false,
                  updated: false,

            }
      }
      componentDidMount() {
            axios.get(`${config.url.baseURL}/booking/${this.state.userId}`)
                  .then((response) => {
                        var ticket = []
                        var length = response.data.length;
                        ticket = response.data[length - 1];
                        this.setState({
                              ticketDetail: ticket,
                              updated: true,
                        })

                  })
      }

      render() {
            return (
                  <div>
                        <br></br>
                        <h4 className='successMessage'>Payment Succesfull!</h4>
                        <div>
                              <div className='inputForm'>
                                    <div className="card" id='inputFormCard'>
                                          <div className="card-header" id='inputFormHeader'>
                                                Ticket
                                          </div>
                                          <div className="card-body" id='inputFormBody'>
                                                <form >
                                                      <div className="form-group">
                                                            <label name='id'>Ticket Number : </label>{randomstring.generate({
                                                                  length: 10,
                                                                  charset: 'numeric',
                                                                  capitalization: 'lowercase'
                                                            })}
                                                      </div>
                                                      <div className="form-group">
                                                            <label name='username'>Booked Name : </label>{this.state.userName}
                                                      </div>
                                                      <div className="form-group">
                                                            <label name='theater'>Theater Name : </label>{this.state.ticketDetail.theater}
                                                      </div>
                                                      <div className="form-group">
                                                            <label name='movie'> Movie Name : </label>{this.state.ticketDetail.movie}
                                                      </div>
                                                      <div className="form-group">
                                                            <label name='showTime'> Show Time : </label>{this.state.ticketDetail.showTime}
                                                      </div>
                                                      <div className="form-group">
                                                            <label name='seat'> Seats Booked : </label>{this.state.ticketDetail.tickets}
                                                      </div>
                                                </form>
                                          </div>
                                    </div>
                              </div><Link to="/book"><p className='text' >Book Another Ticket.</p></Link></div>
                  </div>);


      }
}
export default Ticket