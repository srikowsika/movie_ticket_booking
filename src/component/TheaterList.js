import React from 'react';
import axios from 'axios';
import { Link } from "react-router-dom";
import '../assets/stylesheets/stylesheet.css';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
import config from '../config/config';


class TheaterList extends React.Component {
  constructor() {
    super();
    this.state = {
      theaterDetails: [],
      number: 1,
      status: ''
    }
  }
  componentDidMount() {
    axios.get(`${config.url.baseURL}/theater`)
      .then(function (response) {

        this.setState({
          theaterDetails: response.data,
        })
      }.bind(this));
  }

  deleteTheater(theaterId) {
    axios.delete(`${config.url.baseURL}/theater/${theaterId}`)

    axios.delete(`${config.url.baseURL}/showTime/theater/${theaterId}`)
      .then((response) => {
        if (response.status === 200) {
          this.setState({
            status: true,
          })
          this.props.history.push({
            pathname: '/admin/theaterList',
            state: { detail: 'true' }
          })

        }
      }
      )
  }
  remove = (theaterId) => {
    confirmAlert({

      message: 'Are you sure to delete this Theater?',
      buttons: [
        {
          label: 'Yes',
          onClick: () => this.deleteTheater(theaterId)
        },
        {
          label: 'No',
        }
      ]
    });
  };
  confrimBox = () => {
    confirmAlert({

      message: 'Theater Added Succesfullly',
      buttons: [
        {
          label: 'OK',

        },

      ]

    });
  };
 
  render() {
    return (
      <div >
        {this.props.location.state !== undefined ?
          this.props.location.state.detail !== 'true' ? <div className="alert alert-success alert-dismissible fade show" role="alert">
            <p className='text'><strong>{this.props.location.state.detail} </strong>Added Succesfully</p>
            <button type="button" className="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div> : null : null}
        {this.props.location.state !== undefined ?
          this.props.location.state.detail === 'true' ? <div className="alert alert-success alert-dismissible fade show" role="alert">
            <p className='text'>Deleted Succesfully</p>
            <button type="button" className="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div> : null : null}


        <br></br>
        <ul className='theater'>
          <li><Link to='/admin/dashboard' >Back</Link></li>
          <li> <Link to='/admin/theaterAdd' id='add' >Add Theater</Link>
          </li>
        </ul>

        <table className="table table-striped table-responsive " id='table'>

          <thead className="thead-light">
            <tr>
              <th scope="col">TheaterName</th>
              <th scope="col">Delete theater</th>
            </tr>
          </thead>
          <tbody>
            {
              this.state.theaterDetails.map((detail) =>
                <tr key={detail._id} >
                  <td><Link to='/admin/theater' id='ticketbutton' onClick={() => localStorage.setItem("theaterId", detail._id)}>{detail.theaterName}</Link></td>
                  <td><button id='ticketbutton' onClick={() => this.remove(detail._id)}>Remove</button></td>
                </tr>)
            }
          </tbody>
        </table>
      </div>
    )
  }
}

export default TheaterList;

