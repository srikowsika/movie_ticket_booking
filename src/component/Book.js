import React, { Component } from 'react';
import '../assets/stylesheets/stylesheet.css';
import MovieCard from './MovieCard';
import { Link, Redirect } from 'react-router-dom';
import axios from 'axios';
import config from '../config/config';


class Book extends Component {


  constructor(props) {
    super(props);

    this.state = {
      theaterId: localStorage.getItem("theaterId"),
      movieId: localStorage.getItem("movieId"),
      userId: localStorage.getItem('userId'),
      theaterName: localStorage.getItem("theaterName"),
      movieName: localStorage.getItem("movieName"),
      theaterDetails: [],
      showDetails: [],
      price: 0,
      seat: '',
      status: false,
      onClick: false,
    }
    //this.amountCalculation = this.amountCalculation.bind(this);
  }

  componentDidMount() {
    axios.get(`http://localhost:5000/theater/theaterId/${this.state.theaterId}`)
      .then(function (response) {
        if (response.status === 200) {
          this.setState({
            theaterDetails: response.data,
          })
        }

      }.bind(this))

    axios.get(`http://localhost:5000/showTime`)
      .then(function (response) {
        if (response.status === 200) {
          response.data.map((detail) => {
            if (detail.theaterId === this.state.theaterId && detail.movieId === this.state.movieId) {

              localStorage.setItem("showId", detail._id);
              this.setState({
                showDetails: detail,
              })

            }
            return null;
          }
          )
        }

      }.bind(this))
  }
  bookTicket(event) {
    event.preventDefault();
    var bookingDetails = {
      "theater": this.state.theaterName,
      "movie": this.state.movieName,
      "showTime": this.state.showDetails.showTime,
      "tickets": this.state.seat,
    }
    axios.post(`${config.url.baseURL}/booking/${this.state.userId}`, { bookingDetails })
      .then((response) => {

        localStorage.setItem("bookingId", response.data.bookingData._id)
        this.setState({
          status: true
        })
      })
    axios.put(`${config.url.baseURL}/showTime/${this.state.showDetails._id}/${this.state.seat}`, {})
  }

  findTheater() {
    var theaterName;
    this.state.theaterDetails.map((theater) => {

      theaterName = theater.theaterName;
      localStorage.setItem("theaterName", theaterName);

      return null;
    })
    return <li>{theaterName}</li>
  }

  amountCalculation(e) {
    e.preventDefault();
    this.setState({
      onClick: true,
    })
    if (this.state.seat <= this.state.showDetails.availability) {
      this.setState({
        price: 120 * this.state.seat,
        theaterName: localStorage.getItem("theaterName"),
      })
    }
    else {
      this.setState({ price: -1 })
    }

  }
  render() {

    if (this.state.status === true) {

      return (<Redirect to='/ticket' />)
    }
    return (
      <div>
        <br></br>
        <ul>
          <li>
            <Link to='/theater'>Back</Link>
          </li>
        </ul>
        <div>
          <div className="container" id="movieContainer">
            <div className="row" id='row'>
              <div className='col'>
                <MovieCard />
              </div>
              <div className='col-md-4 ml-0 mr-0 col-lg-3' id='col'>
                <div className='card-title'><h2>Book Tickets</h2></div>
                {this.state.onClick === false ?
                <form  onSubmit={this.amountCalculation.bind(this)}>
                    <div className="form-group">
                      <label >Seat Required</label>
                      <input type="number" min='1' onChange={event => this.setState({ seat: event.target.value })} className="form-control" id="inputSeat" required /></div>
                      <div className="form-group"><label> Total Amount : {this.state.seat * 120}</label></div>
                      {this.state.seat > this.state.showDetails.availability ?
                            <p className='errorMessage'>Your requirement exceeds the availability</p> :<button className="btn btn-primary">Book Now</button>}
                  </form>  :       
                  <form>
                      <div>Ticket booked {this.state.seat}<br></br><legend></legend>Total Amount {this.state.price}<br></br><legend></legend><span> <legend></legend><button className="btn btn-primary" onClick={this.bookTicket.bind(this)}>Pay Now</button></span></div>
                  </form>}

                        
                          
                           {/* //</div>this.state.seat > 0 && this.state.seat <= this.state.showDetails.availability ? */}
                          {/* //   <div> <div>
                          //     <div></div> :      
                    //     }</div> </div> : null}</div> : null */}
                    </div>
            </div>
          </div>
        </div>
        <table className="table table-striped ">
          <thead className="thead-light">
            <tr>
              <th scope="col">TheaterName</th>
              <th scope="col">ShowTime</th>
              <th scope="col">Seat Available</th>
              <th scope="col">Price</th>
            </tr>
          </thead>
          <tbody>
            {
              <tr>
                <td>{this.findTheater()}</td>
                <td>{this.state.showDetails.showTime}</td>
                <td>{this.state.showDetails.availability}</td>
                <td>120</td>
              </tr>
            }
          </tbody>
        </table>
      </div>
    );
  }
}

export default Book


