import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import config from '../config/config';


class TheaterAdd extends Component {

    constructor(props) {
        super(props);
        this.state = {
            cityDetails: [],
            cityId: '',
            theaterName: '',
            theaterId: null,
            theaterDetails: [],
            movieDetails: [],
            movieId: '',
            showTime: '',
            availabilty: 0,
            status: null,
            isEmpty: false

        }

    }
    componentDidMount() {
        axios.get(`${config.url.baseURL}/city`)
            .then(function (response) {
                if (response.status === 200) {
                    this.setState({
                        cityDetails: response.data,
                    })
                }
            }.bind(this))

        axios.get(`${config.url.baseURL}/movie`)
            .then(function (response) {

                this.setState({
                    movieDetails: response.data,
                })
            }.bind(this))
    }
    postTheater(event) {
        event.preventDefault();
        
            var postTheater = {
                "theaterName": this.state.theaterName
            }
            axios.post(`${config.url.baseURL}/theater/${this.state.cityId}`, { postTheater })
                .then((response) => {
                    this.setState({
                        status: true
                    })
                    if (this.state.status === true) {
                        this.props.history.push({
                            pathname: '/admin/theaterList',
                            state: { detail: this.state.theaterName }
                        })
                    }
                })
                .catch((err) => {
                    console.log(err)
                    this.setState({
                        status: false
                    })
                }

                )
        
    }

    render() {
        
        return (
            <div>
                <div className='inputForm'>
                    <div className="card border-light mb-3" id='inputFormCard'>
                        <div className="card-header" id='inputFormHeader'>
                            Add Theater
            </div>
                        <div className="card-body" id='inputFormBody'>
                            <form onSubmit={this.postTheater.bind(this)} >
                                <div className="form-group">
                                    <label>Theater Name </label>
                                    <input type='text' className="form-control" minLength='2' maxLength='20' placeholder='Enter the theater name' pattern='[A-Za-z,]*' onChange={event => this.setState({ theaterName: event.target.value })} required />
                                </div>
                                <div className="form-group">
                                    <label>Select the city </label>
                                    <select className="form-control" onChange={event => this.setState({ cityId: event.target.value })} required >
                                        <option>Select the city</option>
                                        {
                                            this.state.cityDetails.map((city) =>
                                                <option key={city._id} value={city._id}>{city.city}</option>
                                            )}
                                    </select >
                                </div>
                                {this.state.status === false ? <p className='errorMessage'>theater already Exists</p> : null}
                                {this.state.isEmpty === true ? <p className='errorMessage'>Please fill all the fields</p> : null}
                                <button className="btn btn-primary" id='adminbutton'>Add</button>
                                <Link to='/admin/theaterList'> <button className="btn btn-primary">Cancel</button></Link>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        );

    }
}
export default TheaterAdd;