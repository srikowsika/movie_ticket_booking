import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import '../assets/stylesheets/stylesheet.css';
import { confirmAlert } from 'react-confirm-alert';
import config from '../config/config';


class Theater extends Component {

    constructor(props) {
        super();
        this.state = {
            theaterId: localStorage.getItem("theaterId"),
            showDetail: {},
            theaterDetails: {},
            movieDetails: [],
            status: false,
            isDeleted: false,
            cityId: null
        }
    }

    componentDidMount() {
        axios.get(`${config.url.baseURL}/showTime/show/${this.state.theaterId}`)
            .then(function (response) {
                if (response.data.length !== 0) {
                    this.setState({
                        showDetail: response.data,
                        status: true,
                    })

                }
            }.bind(this))

        axios.get(`${config.url.baseURL}/theater/theaterId/${this.state.theaterId}`)
            .then(function (response) {

                Object.assign({}, response.data)
                this.setState({
                    theaterDetails: response.data[0],
                    cityId: response.data[0].cityId
                })
            }.bind(this))

        axios.get(`${config.url.baseURL}/movie`)
            .then(function (response) {
                this.setState({
                    movieDetails: response.data,
                })
            }.bind(this))
    }

    findMovieName(movieId) {
        var movieName;
        this.state.movieDetails.map((movie) => {
            if (movie._id === movieId) {
                movieName = movie.movieName;

            }
            return null;
        })
        return <p>{movieName}</p>
    }
    deleteMovie(showId) {
        axios.delete(`${config.url.baseURL}/showTime/${showId}`)
            .then(response => {
                if (response.status === 200) {
                    this.setState({
                        isDeleted: true
                    })
                    if (this.state.isDeleted === true) {
                        this.props.history.push({
                            pathname: '/admin/theater',
                            state: { detail: true }
                        })
                    }
                }
            })
    }
    remove = (showId) => {
        confirmAlert({

            message: 'Are you sure to delete this movie?',
            buttons: [
                {
                    label: 'Yes',
                    onClick: () => this.deleteMovie(showId)
                },
                {
                    label: 'No',
                }
            ]
        });
    };

    render() {

        return (
            <div>

                {this.props.location.state !== undefined ?
                    this.props.location.state.detail === 'posted' ? <div className="alert alert-success alert-dismissible fade show" role="alert">
                        <p className='text'>Movie Added Succesfully</p>
                        <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div> : null : null}
                {this.props.location.state !== undefined ?
                    this.props.location.state.detail === true ? <div className="alert alert-success alert-dismissible fade show" role="alert">
                        <p className='text'>Deleted Succesfully</p>
                        <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div> : null : null
                }
                <br></br>
                <ul className='theater'>
                    <li><Link to='/admin/theaterList' >Back</Link></li>
                    <li><b>Theater Name : {this.state.theaterDetails.theaterName}</b></li>
                    <li><Link to='/admin/theater/movie' id='add'>Add Movie</Link></li>
                </ul>
                {this.state.status === true ?
                    <table className="table table-striped ">
                        <thead className="thead-light">
                            <tr>
                                <th scope="col">Movie Name</th>
                                <th scope="col">Show Time</th>
                                <th scope="col">Availability</th>
                                <th scope="col">Delete Movie</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.state.showDetail.map((show) =>
                                    <tr key={show._id}>
                                        <td>{this.findMovieName(show.movieId)}</td>
                                        <td>{show.showTime}</td>
                                        <td>{show.availability}</td>
                                        <td><button id='ticketbutton' onClick={() => this.remove(show._id)}>Remove</button></td>
                                    </tr>
                                )

                            }
                        </tbody>
                    </table> : <div className='text'>No movies Found</div>}
                    <div className='pagination'>
                        <span>&laquo;</span>
                        <span className='styles.active'>1</span>
                        <span>2</span>
                        <span>3</span>
                        <span>4</span>
                    </div>

            </div>
        )
    }


}
export default Theater;