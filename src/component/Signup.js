import React, { Component } from 'react';
import { Link } from "react-router-dom";
import '../assets/stylesheets/stylesheet.css';
import axios from 'axios';
import Signin from '../component/Signin'
import config from '../config/config';

class Signup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            email: '',
            password: '',
            confirmPassword: '',
            isRegistered: null,
            isEmpty: false
        }
    }

    registerUser(event) {
        event.preventDefault();
        if (this.state.name === '' || this.state.email === '' || this.state.password === '' || this.state.confirmPassword === '') {
            this.setState({
                isEmpty: true
            })
        }
        else {
            if (this.state.password === this.state.confirmPassword) {

                var newUser = {
                    "signupName": this.state.name,
                    "signupEmail": this.state.email,
                    "signupPassword": this.state.password,
                    "conformPassword": this.state.confirmPassword
                }

                axios.post(`${config.url.baseURL}/signup`, { newUser })
                    .then(function (response) {

                        if (response.status === 200) {
                            this.setState({
                                isRegistered: true,
                            });
                        }
                    }.bind(this))

                    .catch(function (error) {
                        this.setState({
                            isRegistered: false
                        })
                    }.bind(this))
            }
            else {
                this.setState({
                    passwordValid: false
                })
            }
        }
    }

    render() {

        if (this.state.isRegistered === true) {
            return (<Signin value='1' />)
        }
        if (this.state.isRegistered === false) {
            return <Signup value='1' />
        }
        return (
            <div>

                <div className='inputForm'>
                    <div className="card border-light mb-3" id='inputFormCard'>
                        <div className="card-header" id='inputFormHeader'>
                            Register Form
                </div>
                        <div className="card-body border-top border-bottom-0" id='inputFormBody'>
                            <form onSubmit={this.registerUser.bind(this)} >

                                <div className="form-group">
                                    <label>Username</label>
                                    <input className='form-control' type="text" title="Please follow the following format KVAnadh or K.V Anadh" minLength="3" maxLength="20" pattern="^[a-zA-Z]+(([. -][a-zA-Z ])?[a-zA-Z]*)*$" name="name" placeholder="Enter your name" onChange={event => this.setState({ name: event.target.value })} required />
                                </div>
                                <div className="form-group">
                                    <label>User Email-id</label>
                                    <input type="email" className='form-control' name="email" placeholder="Enter your email" onChange={event => this.setState({ email: event.target.value })} required />
                                </div>
                                <div className="form-group">
                                    <label>Password</label>
                                    <input type='password' className='form-control' minLength="8" maxLength="15" name='password' placeholder="Password 8-15 character"
                                        onChange={event => this.setState({ password: event.target.value })} required />
                                </div>
                                <div className="form-group">
                                    <label>Confirm Password</label>
                                    <input type='password' className='form-control' minLength="8" maxLength="15" name='password' placeholder="Re-enter your passsword"
                                        onChange={event => this.setState({ confirmPassword: event.target.value })} required />
                                </div>
                                {this.state.passwordValid === false ? <p className='errorMessage'>Password mismatch</p> : null}
                                {this.props.value === '1' ? <p className='errorMessage'>User already exist</p> : null}
                                {this.state.isEmpty === true ? <p className='errorMessage'>Please fill all the fields</p> : null}
                                <button className="btn btn-primary" id='userbutton'
                                >Register</button>
                                <legend></legend>
                                <p className='text'>Already a member? <Link to='/signin'>Sign in</Link></p>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        );

    }

}

export default Signup;