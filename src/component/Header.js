import React from 'react';
import '../assets/stylesheets/stylesheet.css';
import { Link } from "react-router-dom";
class Header extends React.Component {
  render() {
    return (
      <div>
        <div className="header">
          {localStorage.getItem("usertype") === "1" ?
            <nav className="navbar navbar-light" >
              <Link className="navbar-brand" to="/admin/dashboard">
                <img className="logo" src={require('../assets/images/logo.jpeg')} width="30" height="40" alt="" /></Link>
              <h1 className="mx-auto"> <Link className="text-dark nav-link" to="/admin/dashboard"  >Movie Ticket Booking</Link></h1></nav> : <nav className="navbar navbar-light"> <Link className="navbar-brand" to="/">
                <img className="logo" src={require('../assets/images/logo.jpeg')} width="30" height="40" alt="" /></Link>
              <h1 className="mx-auto"><Link className="text-dark nav-link" to='/' >Movie Ticket Booking</Link></h1></nav>
          }
        </div>


      </div>
    );
  }
}
export default Header;