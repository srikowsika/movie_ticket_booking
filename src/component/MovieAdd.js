import React from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import config from '../config/config';


class MovieAdd extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      movieName: '',
      movieDirector: '',
      movieGenre: '',
      movieImage: '',
      movieDetails: '',
      status: null,
      isEmpty: false,
    }


  }

  addMovie(event) {
    event.preventDefault();
    if (this.state.movieName === '' || this.state.movieDirector === '' || this.state.movieGenre === '' || this.state.movieImage === '') {
      this.setState({
        isEmpty: true
      })
    }

    else {
      this.setState({
        isEmpty: false,
      })
      var postMovie = {
        "movieName": this.state.movieName,
        "director": this.state.movieDirector,
        "musicDirector": this.state.movieGenre,
        "image": this.state.movieImage,

      }
      axios.post(`${config.url.baseURL}/movie`, { postMovie })
        .then((response) => {
          this.setState({
            status: true,
          })
          this.props.history.push({
            pathname: '/admin/movie',
            state: { detail: this.state.movieName }
          })
        })
        .catch((response) => {
          this.setState({
            status: false
          })
        })
    }


  }
  imageChange(e) {
    var file = e.target.files[0];
    var myReader = new FileReader();
    var extension = file.name.split('.');

    if (extension[1] === 'jpg' || extension[1] === 'png' || extension[1] === 'blog' || extension[1] === 'jpeg') {
      myReader.onloadend = (e) => {

        this.setState({
          movieImage: myReader.result
        });
      }
      myReader.readAsDataURL(file);
    }
    else {
      this.setState({
        movieImage: false
      })
    }

  }

  render() {
    return (
      <div>
        <div className='inputForm'>
          <div className="card border-light mb-5" id='inputFormCard'>
            <div className="card-header" id='inputFormHeader'>
              Add New Movie
              </div>
            <div className="card-body border-top border-bottom-0" id='inputFormBody'>
              <form onSubmit={this.addMovie.bind(this)}>
                <div className="form-group">
                  <label>Movie name</label>
                  <input type='text' className='form-control' name='movieName' maxLength='20' pattern="[A-Za-z0-9]*" placeholder='Enter the movie' size='5' onChange={event => this.setState({ movieName: event.target.value })} required />

                </div>
                <div className="form-group">
                  <label>Movie Director </label>
                  <input type='text' className='form-control' name='director' pattern="^[a-zA-Z]+(([. -][a-zA-Z ])?[a-zA-Z]*)*$" title="Please follow the following format K.V Anadh" placeholder='Enter the director' size='5' onChange={event => this.setState({ movieDirector: event.target.value })} required />
                </div>
                <div className="form-group">
                  <label>Movie Genre </label>
                  <input type='text' className='form-control' name='musicDirector' placeholder='Enter the genre' size='5' onChange={event => this.setState({ movieGenre: event.target.value })} required />
                </div>
                <div className="form-group">
                  <label>Insert image </label>

                  <input type="file" name="myImage" accept=".png, .jpg, .jpeg" className="form-control-file" id="exampleFormControlFile1" onChange={this.imageChange.bind(this)} required />
                  <legend></legend>
                </div>
                {this.state.movieImage === false ?
                  <p className='errorMessage'>Incorrect file format</p> :
                  <React.Fragment>{this.state.movieImage !== '' ?
                    <div><img className="card-img-top" src={this.state.movieImage} alt='' /><legend></legend></div> : null}
                    {this.state.status === false ? <p className='errorMessage'>Movie already Exists</p> : null}
                    {this.state.isEmpty === true ? <p className='errorMessage'>Please fill all the fields</p> : null}
                    <button className="btn btn-primary" id='adminbutton'>Submit</button></React.Fragment>}
                <Link to='/admin/movie'> <button className="btn btn-primary">Cancel</button></Link>

              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }


}
export default MovieAdd;
