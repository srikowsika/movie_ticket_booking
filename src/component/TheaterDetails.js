import React, { Component } from 'react';
import '../assets/stylesheets/stylesheet.css';
import { Link } from 'react-router-dom';
import axios from 'axios';
import MovieCard from '../component/MovieCard';
import config from '../config/config';

class TheaterDetails extends Component {

  constructor(props) {
    super(props)
    this.state = {
      movieId: localStorage.getItem("movieId"),
      theaterDetails: [],
      showDetails: [],
      cityDetails: []
    };
    this.findTheater = this.findTheater.bind(this);
    this.findCity = this.findCity.bind(this);
  }

  componentDidMount() {

    axios.get(`${config.url.baseURL}/showTime/movieId/${this.state.movieId}`)

      .then(function (response) {
        if (response.status === 200) {
          this.setState({
            showDetails: response.data
          });
        }
      }.bind(this)
      )

    axios.get(`${config.url.baseURL}/theater`)
      .then(function (response) {
        if (response.status === 200) {
          this.setState({
            theaterDetails: response.data
          })
        }
      }.bind(this)
      )
    axios.get(`${config.url.baseURL}/city`)
      .then(function (response) {
        if (response.status === 200) {
          this.setState({
            cityDetails: response.data
          })
        }
      }.bind(this)
      )
  }

  findTheater(theaterId) {
    var theaterName;
    this.state.theaterDetails.map((theater) => {
      if (theater._id === theaterId) {
        theaterName = theater.theaterName;
      }
      return null;
    })
    return <div>{theaterName}</div>
  }

  findCity(cityId) {
    var cityName;
    this.state.cityDetails.map((city) => {
      if (city._id === cityId) {
        cityName = city.city;
      }
      return null;
    })
    return <div>{cityName}</div>
  }




  render() {
    
    return (

      <div>
        <br></br>
        <ul><li><Link to='/movie'>Back</Link></li></ul>
        <MovieCard />
        {this.state.showDetails.length !==0 ?
        <table className="table table-striped ">
          <thead className="thead-light">
            <tr>
              <th scope="col">TheaterName</th>
              <th scope="col">Location</th>
              <th scope="col">ShowTime</th>
              <th scope="col">Seat Available</th>
              <th scope="col">Book</th>
            </tr>
          </thead>
          <tbody>
            {
              this.state.showDetails.map((detail) =>
                <tr key={detail._id}>

                  <td >{this.findTheater(detail.theaterId)}</td>
                  <td>{this.findCity(detail.cityId)}</td>
                  <td>{detail.showTime}</td>
                  <td>{detail.availability}</td>
                  {detail.availability !== 0 ? <td><Link to='/book' onClick={(event) => { localStorage.setItem("theaterId", detail.theaterId) }}>Book Now</Link></td> :
                    <td className='errorMessage'>House Full</td>}
                </tr>)
            }
          </tbody>
        </table>: <p className='text'>No Theaters Available</p>}

      </div>);

  }
}
export default TheaterDetails;









