import React, { Component } from 'react';


class Movies extends Component {

  render() {
    return (
      <div className="carousel slide" data-ride="carousel">
        <div className="carousel-inner">
          <div className="carousel-item active">
            <img className="d-block ml-4 w-100" src={require('../assets/images/carousel2.jpg')} alt="First slide" />
          </div>
          <div className="carousel-item">
            <img className="d-block ml-4 w-100" src={require('../assets/images/carousel3.jpg')} alt="Second slide" />
          </div>
          <div className="carousel-item">
            <img className="d-block ml-4 w-100" src={require('../assets/images/carousel2.jpg')} alt="Third slide" />
          </div>
        </div>
        <a className="carousel-control-prev" href="/" role="button" data-slide="prev">
          <span className="sr-only">Previous</span>
        </a>
        {/* <a className="carousel-control-next" href="/" role="button" data-slide="next">
          <span className="sr-only">Next</span> */}
       {/* </div> </a> */}
      </div>
    );
  }
}


export default Movies;

