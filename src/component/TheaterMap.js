import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import '../assets/stylesheets/stylesheet.css';
import config from '../config/config';

class TheaterMap extends Component {


    constructor() {
        super();

        this.state = {
            theaterId: localStorage.getItem("theaterId"),
            theaterDetails: [],
            movieDetails: [],
            movieId: '',
            theaterName: '',
            seatAvailable: '',
            showTime: '',
            status: null
        }
        this.timeConversion = this.timeConversion.bind(this)
    }
    componentDidMount() {
        axios.get(`${config.url.baseURL}/theater/theaterId/${this.state.theaterId}`)
            .then((response) => {
                this.setState({
                    theaterDetails: response.data,

                })
                this.state.theaterDetails.map((theater) => {
                    this.setState({
                        cityId: theater.cityId,
                        theaterName: theater.theaterName,
                    })
                    return null;

                })

            })
        axios.get(`${config.url.baseURL}/movie`)
            .then(function (response) {

                this.setState({
                    movieDetails: response.data,
                })
            }.bind(this))
    }

    postShowTime(event) {
        event.preventDefault();
        if (this.state.cityId === '' || this.state.theaterId === '' || this.state.movieId === '' || this.state.seatAvailable === '' || this.state.showTime === '') {
            this.setState({
                status: false
            })
        }
        else {
            var postShowTime = {
                "cityId": this.state.cityId,
                "theaterId": this.state.theaterId,
                "movieId": this.state.movieId,
                "showTime": this.state.showTime,
                "availability": this.state.seatAvailable,
            }
            axios.post(`${config.url.baseURL}/showTime`, { postShowTime })

                .then((response) => {
                    this.setState({
                        status: true,
                    })
                    if (this.state.status === true) {
                        this.props.history.push({
                            pathname: '/admin/theater',
                            state: { detail: 'posted' }
                        })
                    }

                })
        }
    }

    timeConversion(showTime) {
        showTime = showTime.split(':');
        if ((showTime[0] <= 11 && showTime[1] <= 59) && showTime[0] !== '00') {
            var time = showTime[0] + ':' + showTime[1] + 'AM'
            this.setState({
                showTime: time
            })

        }
        else if (showTime[0] === '00' && showTime[1] <= 59) {
            time = 12 + ':' + showTime[1] + 'AM'
            this.setState({
                showTime: time
            })
        }
        else if (showTime[0] === '12' && showTime[1] <= 59) {
            time = 12 + ':' + showTime[1] + 'PM'
            this.setState({
                showTime: time
            })
        }
        else {
            time = (showTime[0] - 12) + ':' + showTime[1] + 'PM'
            this.setState({
                showTime: time
            })
        }

    }
    render() {

        return (
            <div>
                <div className='inputForm'>
                    <div className="card border-light mb-3" id='inputFormCard'>
                        <div className="card-header" id='inputFormHeader'>
                            Add Movie to Theater
                         </div>
                        <div className="card-body border-top border-bottom-0" id='inputFormBody'>
                            <form onSubmit={this.postShowTime.bind(this)}>
                                <div >
                                    <p> Theater Name : {this.state.theaterName}</p>
                                </div>
                                <div className="form-group">
                                    <label>Movie </label>
                                    <select className="form-control" onChange={event => this.setState({ movieId: event.target.value })} required >
                                        <option>Select the Movie</option>
                                        {
                                            this.state.movieDetails.map((movie) =>
                                                <option value={movie._id} key={movie._id}>{movie.movieName}</option>
                                            )
                                        }
                                    </select >
                                </div>
                                <div className="form-group">
                                    <label>Showtime </label>
                                    <input type='time' name="time" min='01:00' className='form-control' onChange={event => this.timeConversion(event.target.value)} required />
                                </div>
                                <div className="form-group">
                                    <label>Seat Avalability </label>
                                    <input type='number' min='1' className='form-control' onChange={event => this.setState({ seatAvailable: event.target.value })} required />

                                </div>
                                {this.state.status === false ? <p className='errorMessage'>Fill all the fields</p> : null}
                                {(this.state.seatAvailable <= 0 && this.state.seatAvailable !== '') || this.state.seatAvailable > 150 ? <p className='errorMessage'>Enter valid seat count your seat should be within 1 - 150</p> :
                                    <div><button className="btn btn-primary" id='adminbutton'>Submit</button>
                                        <Link to='/admin/theater'> <button className="btn btn-primary">Cancel</button></Link><br></br>
                                        <legend></legend>
                                        <Link to='/admin/movieAdd' id='adminbutton'> Add new Movie?</Link></div>}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );

    }
}
export default TheaterMap;