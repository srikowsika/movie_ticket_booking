import React, { Component } from 'react';
import axios from 'axios';
import { Link } from "react-router-dom";
import '../assets/stylesheets/stylesheet.css';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
import config from '../config/config';

class City extends Component {

  constructor() {
    super();
    this.state = {
      city: "null",
      status: "false",
      update: '',
      cityDetails: [],
    }
  }
  componentDidMount() {
    axios.get(`${config.url.baseURL}/city`)
    .then((response) => {
        this.setState({
          cityDetails: response.data,
        })

      })
       .catch((err) =>{
         
       })
  }

  deleteCity(cityId) {
    axios.delete(`${config.url.baseURL}/city/city/${cityId}`)
      .then((response) => {
      })
    axios.delete(`${config.url.baseURL}/theater/city/${cityId}`)
      .then((response) => {

      })
    axios.delete(`${config.url.baseURL}/showTime/city/${cityId}`)
      .then((response) => {

        this.setState({
          status: true
        })
        this.props.history.push({
          pathname: '/admin/city',
          state: { detail: 'true' }
        })
      })

  }
  remove = (cityId) => {
    confirmAlert({

      message: 'Are you sure to delete this City?',
      buttons: [
        {
          label: 'Yes',
          onClick: () => this.deleteCity(cityId)
        },
        {
          label: 'No',
        }
      ]
    });
  };

  render() {
    return (
      <div >
        {this.props.location.state !== undefined ?
          this.props.location.state.detail !== 'true' ?
            <div className="alert alert-success alert-dismissible fade show" role="alert">
              <p className='text'><strong>{this.props.location.state.detail} </strong>Added Succesfully</p>
              <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div> : null : null}
        {this.props.location.state !== undefined ?
          this.props.location.state.detail === 'true' ? <div className="alert alert-success alert-dismissible fade show" role="alert">
            <p className='text'>Deleted Succesfully</p>
            <button type="button" className="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div> : null : null
        }
        <br></br>
        <ul className='theater'>
          <li><Link to='/admin/dashboard' >Back</Link></li>
          <li ><Link to='/admin/city/add' id='add'>Add City</Link></li>
        </ul>
        <table className="table table-striped table-responsive " id='table'>

          <thead className="thead-light">
            <tr>
              <th scope="col">City</th>
              <th scope="col">Remove City</th>
            </tr>
          </thead>
          <tbody>
            {this.state.cityDetails.map((city) =>
              <tr key={city._id} >
                <td>{city.city}</td>
                <td><button id='ticketbutton' onClick={() => this.remove(city._id)}>Remove</button></td>
              </tr>)
            }
          </tbody>
        </table>
      </div>);
  }
}

export default City;
