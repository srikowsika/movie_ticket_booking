import React, { Component } from 'react';
import { Redirect } from "react-router-dom";
import '../assets/stylesheets/stylesheet.css';
import axios from 'axios';
import config from '../config/config';


class MovieDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLogin: true,
      movieDetails: [],
      status: false,
    };
    this.setMovieId = this.setMovieId.bind(this);

  }

  componentDidMount() {
    axios.get(`${config.url.baseURL}/movie`)

      .then(function (response) {
        if (response.status === 200) {

          this.setState({
            movieDetails: response.data,
          });
        }
      }.bind(this));

  }
  setMovieId(movieId, movieName) {

    localStorage.setItem("movieId", movieId);
    localStorage.setItem("movieName", movieName);
    this.setState({
      status: true,
    });

  }

  render() {
    //console.log(this.props)
    if (this.state.status === true) {
      return (<Redirect exact to="/theater" />)
    }
    if (this.state.isLogin === false) {
      return (<Redirect exact to="/signin" />)
    }
    
    return (
      <div>

        <div>
          <div className='container'>
            <div className='row'>
              {
                this.state.movieDetails.map(movie => (
                  <div className="col-md-6 ml-0 mr-0 col-lg-3" key={movie._id}>
                    <div className="card h-70" >
                      {localStorage.getItem("token") ? <img className="card-img-top" onClick={() => this.setMovieId(movie._id, movie.movieName)} src={movie.image} alt="Card  cap" /> : <img className="card-img-top" onClick={() => this.setState({ isLogin: false })} src={movie.image} alt="Cardap" />}
                      <div className="card-body ">
                        <p className="card-title">{movie.movieName}</p>
                        {
                          localStorage.getItem("token") ? <button onClick={() => this.setMovieId(movie._id, movie.movieName)} className="btn btn-primary" id="button">Book Now</button>
                            : null
                        }
                      </div>
                    </div>
                  </div>
                ))
              }
            </div>
          </div>
        </div>

        }
    </div>);
  }
}
export default MovieDetails;



