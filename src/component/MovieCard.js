import React, { Component } from 'react';
import '../assets/stylesheets/stylesheet.css';
import axios from 'axios';
import config from '../config/config';


class MovieCard extends Component {

    constructor(props) {
        super(props);
        this.state = {
            movieId: localStorage.getItem("movieId"),
            movieDetails: [],
        };
    }
    componentDidMount() {
        axios.get(`${config.url.baseURL}/movie/${this.state.movieId}`)
            .then(function (response) {
                if (response.status === 200) {
                    this.setState({
                        movieDetails: response.data,
                    });
                }
            }.bind(this));
    }
    render() {
        return (

            <div className='movieCard'>
                {
                    <div className='movieCard'>
                        {this.state.movieDetails.map(movie => (
                            <div key={movie._id}>
                                <div className="container" id="movieContainer">
                                    <div className="row" id="row">
                                        <div className="col-3" id="col">
                                            <img src={movie.image} alt="Card  cap" id='movieCard' />
                                        </div>
                                        <div className="col-4" id="col">
                                            <div >
                                                <h3 >{movie.movieName}</h3>
                                                <p  >{movie.director}</p>
                                                <p  >{movie.musicDirector}</p>
                                            </div>
                                        </div>
                                        <div className='col-md-4 ml-0 mr-0 col-lg-3' id='col'>

                                            <form>

                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>))}

                    </div>}</div>
        );
    }
}
export default MovieCard;