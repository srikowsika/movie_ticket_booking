import React, { Component } from 'react';
import axios from 'axios';
import { Link } from "react-router-dom"
import '../assets/stylesheets/stylesheet.css';
import config from '../config/config';


class CityAdd extends Component {

  constructor() {
    super();
    this.state = {
      status: '',
      city: '',
      isEmpty: false
    }
  }
  postCity(event) {
    event.preventDefault()

    var city = this.state.city;

    axios.post(`${config.url.baseURL}/city`, { city })
      .then((response) => {
       
          this.setState({
            status: true
          })
          if (this.state.status === true) {
            this.props.history.push({
              pathname: '/admin/city',
              state: { detail: this.state.city }
            })
          }
        
      })
      .catch(err => this.setState({
        status: false
      }))
  }

  render() {
    return (
      <div className='inputForm'>
        <div className="card border-light mb-3" id='inputFormCard'>
          <div className="card-header" id='inputFormHeader'>
            Add City
            </div>
          {this.state.status === true ? <p>City added successfully!</p> : null}
          <div className="card-body" id='inputFormBody'>
            <form onSubmit={this.postCity.bind(this)}>
              <div className="form-group">
                <label>City</label>
                <input type='text' className="form-control" id='inputFormip' name='city' placeholder='Enter the city' title="Please enter only alphabets e.g:'Ooty' " pattern="[A-Za-z]*" onChange={event => this.setState({ city: event.target.value })} required />
              </div>
              {this.state.status === false && this.state.city !== '' ? <p className='errorMessage'>City Already Exists</p> : null}
              {this.state.isEmpty === true ? <p className='errorMessage'>Fill the field</p> : null}
              <button className="btn btn-primary" id='adminbutton'>Submit</button>
              <Link to='/admin/city'> <button className="btn btn-primary"> Cancel</button></Link>
            </form>
          </div>
        </div>
      </div>);
  }
}
export default CityAdd;
