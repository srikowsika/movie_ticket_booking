import React, { Component } from 'react';
import '../assets/stylesheets/stylesheet.css'
import { Link, NavLink } from "react-router-dom";


class Menu extends Component {
  render() {
   
    return (

      <div className='header'>
        <nav className="navbar navbar-expand-lg navbar-light ">
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="navbar-collapse collapse w-100 order-1 order-md-0 dual-collapse2" id="navbarNav">
            <ul className="navbar-nav mr-auto">
              <li className="nav-item active">
                {localStorage.getItem("usertype") === "1" ?
                  <NavLink className="nav-link" to="/admin/dashboard">Home</NavLink> : <NavLink className="nav-link" to="/">Home</NavLink>
                }
              </li>
              <li className="nav-item active">
                {localStorage.getItem("usertype") === "1" ?
                  <Link className="nav-link" to="/admin/city">City</Link> : null}
              </li>
              {localStorage.getItem("token") ?
                localStorage.getItem("usertype") === "1" ?
                  <li className="nav-item active">
                    <Link className="nav-link active" to="/admin/theaterList">Theater</Link>
                  </li> :
                  <li className="nav-item active">
                    <Link className="nav-link active" to="/movie">Movies</Link>
                  </li> : null}
              <li className="nav-item active">
                {localStorage.getItem("usertype") === "1" ?
                  <Link className="nav-link" to="/admin/movie">Movie</Link> : null}
              </li>
            </ul>
            <ul className="navbar-nav ml-auto">
              {localStorage.getItem("token") ?
                <div className="dropdown">
                  <li className="nav-item active" id="dropitem">
                    Welcome,{localStorage.getItem('userName')}
                  </li>
                  <div className="dropdown-content">
                    {localStorage.getItem('userType') === 1 ?
                      <Link to='/admin'> <button onClick={() => localStorage.clear()} id='ticketbutton'>Logout</button></Link> : <Link to='/'> <button onClick={() => localStorage.clear()} id='ticketbutton'>Logout</button></Link>}
                  </div>
                </div> : <li className="nav-item active">
                  <Link className="nav-link mr-0" to="/signin">Login</Link>
                </li>}
            </ul>
          </div>
        </nav>

      </div>);
  }
}
export default Menu;



