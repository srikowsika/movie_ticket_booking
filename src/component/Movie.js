import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import '../assets/stylesheets/stylesheet.css';
import { confirmAlert } from 'react-confirm-alert';
import config from '../config/config';


class Movie extends Component {

    constructor() {
        super();
        this.state = {
            movieDetails: [],
            status: false,
            user: this.props
        }
        this.deleteMovie = this.deleteMovie.bind(this)
    }

    componentDidMount() {
        axios.get(`${config.url.baseURL}/movie`)

            .then((response) => {
                this.setState({
                    movieDetails: response.data,
                })
            })
    }
    deleteMovie(movieId) {
        axios.delete(`${config.url.baseURL}/movie/${movieId}`)
            .then((response) => {

            })
        axios.delete(`${config.url.baseURL}/showTime/movie/${movieId}`)
            .then((response) => {
                this.setState({
                    status: true,
                })
                this.props.history.push({
                    pathname: '/admin/movie',
                    state: { detail: 'true' }
                })
            })
    }
    remove = (movieId) => {
        confirmAlert({

            message: 'Are you sure to delete this movie?',
            buttons: [
                {
                    label: 'Yes',
                    onClick: () => this.deleteMovie(movieId)
                },
                {
                    label: 'No',
                }
            ]
        });
    };
    render() {
        return (
            <div>
                {this.props.location.state !== undefined ?
                    this.props.location.state.detail !== 'true' ? <div className="alert alert-success alert-dismissible fade show" role="alert">
                        <p className='text'><strong>{this.props.location.state.detail} </strong>Added Succesfully</p>
                        <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div> : null : null}
                {this.props.location.state !== undefined ?
                    this.props.location.state.detail === 'true' ? <div className="alert alert-success alert-dismissible fade show" role="alert">
                        <p className='text'>Deleted Succesfully</p>
                        <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div> : null : null
                }
                <br></br>
                <ul className='theater'>
                    <li><Link to='/admin/dashboard' >Back</Link></li>
                    <li>
                        <Link to='/admin/movieAdd' id='add'>Add Movie</Link>
                    </li>
                </ul>
                <table className="table table-striped table-responsive " id='table'>
                    <thead className="thead-light">
                        <tr>
                            <th scope="col">Movie Name</th>
                            <th scope="col">Remove Movie</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.state.movieDetails.map((movie) =>
                                <tr key={movie._id} >
                                    <td>{movie.movieName}</td>
                                    <td><button id='ticketbutton' onClick={() => this.remove(movie._id)}>Remove</button></td>
                                </tr>)
                        }
                    </tbody>
                </table>
            </div>);
    }
}
export default Movie;