import React, { Component } from 'react';
import Movies from '../component/Movies'
import MovieDetails from './MovieDetails';

class Home extends Component {
    render() {
        return (
            <div className='home'>
                <Movies />
                <MovieDetails />
            </div>
        )
    }
}
export default Home;