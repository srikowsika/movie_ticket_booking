import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import 'bootstrap/dist/css/bootstrap.min.css';
import './assets/stylesheets/stylesheet.css'


ReactDOM.render(<App />, document.getElementById('root'));


