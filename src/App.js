import React from 'react';
import Header from "../src/component/Header";
import {BrowserRouter} from 'react-router-dom';
import Menu from "../src/component/Menu";
import RouterComponent from './routers/router';



function App() {
return (
    <BrowserRouter >
     <React.Fragment>
        <Header /> 
        <Menu />
        <RouterComponent /> 
      </React.Fragment>
  </BrowserRouter>
);
}

export default App;
