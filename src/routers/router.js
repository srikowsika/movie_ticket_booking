import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import Signin from '../component/Signin';
import Signup from '../component/Signup';
import TheaterDetails from '../component/TheaterDetails'
import Home from '../component/Home';
import Book from '../component/Book';
import AdminHome from '../component/AdminHome'
import MovieAdd from '../component/MovieAdd';
import Ticket from '../component/Ticket';
import TheaterMap from '../component/TheaterMap';
import TheaterAdd from '../component/TheaterAdd';
import MovieDetails from '../component/MovieDetails';
import TheaterList from '../component/TheaterList';
import Theater from '../component/Theater';
import City from '../component/City';
import Dashboard from '../component/Dashboard';
import CityAdd from '../component/CityAdd';
import Movie from '../component/Movie';
import Menu from '../component/Menu'


function RouterComponent() {

  const AdminRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={(props) => (
      localStorage.getItem("token") !== null ? localStorage.getItem("usertype") === "1" ? <Component {...props} />
        : <Redirect to='/signin' />
        : <Redirect exact to='/' />
    )} />
  )

  const UserRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={(props) => (
      localStorage.getItem("usertype") === "0" ?
        <Component {...props} />
        : <Redirect to='/signin' />
    )} />
  )


  return (
    <div>
      <Switch>
        <Route path='/menu' component={Menu}></Route>
        <Route exact path='/' component={Home}></Route>
        <UserRoute path='/movie' component={MovieDetails}></UserRoute>
        <Route exact path='/admin' component={AdminHome}></Route>
        <Route path='/signin' component={Signin}></Route>
        <Route path='/signup' component={Signup} ></Route>
        <Route path='/admin/dashboard' component={Dashboard}></Route >
        <AdminRoute path='/admin/theater/movie' component={TheaterMap}></AdminRoute >
        <AdminRoute path='/admin/movie' component={Movie}></AdminRoute>
        <AdminRoute path='/admin/city/add' component={CityAdd}></AdminRoute>
        <AdminRoute path='/admin/theaterList' component={TheaterList}></AdminRoute>
        <UserRoute path='/ticket' component={Ticket}></UserRoute>
        <UserRoute path='/book' component={Book}></UserRoute>
        <UserRoute path='/theater' component={TheaterDetails}></UserRoute>
        <AdminRoute path='/admin/city' component={City}></AdminRoute>
        <AdminRoute path='/admin/theater' component={Theater}></AdminRoute>
        <AdminRoute path='/admin/theaterAdd' component={TheaterAdd}></AdminRoute >
        <AdminRoute path="/admin/movieAdd" component={MovieAdd}></AdminRoute >
      </Switch>
    </div>);

}
export default RouterComponent; 
